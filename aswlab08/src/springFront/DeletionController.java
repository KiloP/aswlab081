package springFront;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Vector;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import models.ModelException;
import models.Tweet;
import models.User;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import util.TimeService;



@Controller
@RequestMapping("/delete")
@SessionAttributes("deletableTweets")
public class DeletionController {

	private static final Logger logger = LoggerFactory.getLogger(DeletionController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String setupForm(HttpSession session, Model model) {

		Integer userID = (Integer) session.getAttribute("loggedUserID");
		Vector<HashMap<String, Object>> wallTweets = new Vector<HashMap<String, Object>>();
		SimpleDateFormat mySQLTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Collection<Tweet> tweets = Tweet.findByUserId(userID);

			for (Tweet tweet : tweets) {
				HashMap<String,Object> tweetHash = new HashMap<String, Object>();
				tweetHash.put("tweetID", tweet.getTweet_id());
				tweetHash.put("content", tweet.getText());
				tweetHash.put("likes", tweet.getLikes());
				java.util.Date temps = mySQLTimeStamp.parse(tweet.getTime(), new ParsePosition(0));
				tweetHash.put("tweetDate", TimeService.getDate(temps));
				tweetHash.put("tweetHour", TimeService.getHour(temps));
				wallTweets.addElement(tweetHash);
			}

			model.addAttribute("deleteForm", new DeleteForm());
			model.addAttribute("deletableTweets",wallTweets);
			logger.info("Returning tweets to delete");
			return "todelete";
		}
		catch (ModelException ex) {
			model.addAttribute("theList",ex.getMessageList());
			return "error";
		}
	}
	/*
	@ModelAttribute("deleteForm")

	DeleteForm getDeleteForm() {
		return new DeleteForm();
	}
	 */
	@RequestMapping(method = RequestMethod.POST)
	public  String delete(@Valid @ModelAttribute("deleteForm") DeleteForm deleteForm, 
			BindingResult binding, HttpSession session, Model model) 
	{

		logger.info("Deleting tweets... ");
		if (binding.hasErrors()) {
			logger.info("Deletion fails: no valid input");
			return "todelete";
		}

		String[] tweetIDs = deleteForm.getTweetids();
		Integer userID= (Integer) session.getAttribute("loggedUserID");
		String password = deleteForm.getUserpasswd();

		try { 

			User user = User.findById(userID);
			if (user.passwordMatch(password)){
				for (String tid : tweetIDs) {
					Tweet tweet = Tweet.findById(Integer.valueOf(tid));
					if (tweet.getUser_id() == userID)
						tweet.delete();				
				}
				return "redirect:/";
			}
			else {
				String mess = "Wrong password!";
				binding.rejectValue("userpasswd", "wrongPassword", mess);
				logger.info("Deletion fails: wrong password");
				return "todelete";
			}
		}
		catch (ModelException ex) {
			model.addAttribute("theList",ex.getMessageList());
			logger.info("Deletion fails: sql error");
			return "error";
		}
	}
}

